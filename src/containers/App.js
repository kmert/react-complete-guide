import React, { Component } from 'react';
import styles from './App.module.css';

import PersonList from '../components/PersonList/PersonList';
import Cockpit from '../components/Cockpit/Cockpit';

class App extends Component {
  state = {
    persons: [
      {
        id: 'asd1',
        name: 'Max',
        age: 28,
        hobbies: ['Cycling', 'Swimming', 'Hiking']
      },
      {
        id: 'ads1',
        name: 'Manu',
        age: 30,
        hobbies: ['Scuba Diving', 'Climbing', 'Paragliding']
      },
      { id: 'das1', name: 'Stephanie', age: 26, hobbies: [] }
    ],
    showPersonList: false
  };

  deletePersonHandler = index => {
    const persons = [...this.state.persons];
    persons.splice(index, 1);
    this.setState({ persons: persons });
  };

  textChangedHandler = (event, id) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });
    const person = {
      ...this.state.persons[personIndex]
    };
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;
    this.setState({ persons: persons });
  };

  togglePersonListHandler = () => {
    this.setState({ showPersonList: !this.state.showPersonList });
  };

  render() {
    let personList = null;
    if (this.state.showPersonList) {
      personList = (
        <PersonList
          persons={this.state.persons}
          clicked={this.deletePersonHandler}
          changed={this.textChangedHandler}
        />
      );
    }

    return (
      <div className={styles.App}>
        <Cockpit
          clicked={this.togglePersonListHandler}
          persons={this.state.persons}
          show={this.state.showPersonList}
        />
        {personList}
      </div>
    );
  }
}

export default App;
