import React from 'react';
import styles from './Cockpit.module.css';

const cockpit = props => {
  let styleClasses = [];
  let btnClass = '';

  if (props.persons.length <= 2) {
    styleClasses.push(styles.red);
  }
  if (props.persons.length <= 1) {
    styleClasses.push(styles.bold);
  }

  if (props.show) {
    btnClass = styles.Red;
  }
  return (
    <div className={styles.Cockpit}>
      <h1>Hello World!</h1>
      <p className={styleClasses.join(' ')}>Hi! I'm alive!</p>
      <button className={btnClass} onClick={props.clicked}>
        Toggle Name
      </button>
    </div>
  );
};

export default cockpit;
