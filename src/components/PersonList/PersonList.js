import React from 'react';
import Person from '../Person/Person';

const personList = props =>
  props.persons.map((person, index) => {
    return (
      <Person
        name={person.name}
        age={person.age}
        key={person.id}
        itemClicked={props.clicked.bind(index)}
        textChanged={event => props.changed(event, person.id)}
      >
        {person.hobbies.join(', ')}
      </Person>
    );
  });

export default personList;
